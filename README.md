listlp - Print text files
=========================

What is this about?
-------------------

The `listlp` program generates PDF or PostScript output given one
or more text fles.

Usage
-----

The program generates on standard output a ready to print PDF or
PostScript program, which can be the input of your print command.

Syntax is: `listlp ` _options_ [_file1_ .... _filen_]

### Input files

Input files are specified as arguments. If no file is given, standard
input is used.

### Options

| Option | Description | Default |
| :----- | :------------------------ | :-------------- |
| `-b` | do not print banner | banner is printed |
| `-c` | charset to convert from, with `iconv` (`iconv -f `...` -t iso8859-1`) | utf-8 |
| `-f` | PostScript font name for text | Courier |
| `-l` | number of lines per page/column | 66 |
| `-n` | no line numbering | line numbers are printed |
| `-s` | no borders around text | borders are printed |
| `-d` | no date in banner | date is printed |
| `-t` | PostScript font name for title (i.e. banner) | Times-Roman |
| `-o` format | `ps` or `pdf` | `pdf` |
| `-1` | print one column per page (portrait) | two columns |
| `-2` | print two columns per page (landscape) | two columns |


Installation
------------

Prerequisites: standard Unix commands (`awk`, `sed`, `expand`, etc.),
GNU `enscript`
and `ps2pdf` (included with Ghostscript).

Steps: copy the `listlp` to one of your `PATH` directories and make
this program executable (`chmod +x listlp`).

Examples
--------

To print all C source files in current directory to default printer
using 2 columns (thus landscape mode):

```
    listlp *.[ch] | lp
```

To print all sources in subdirectories, omitting the date in the banner,
and using duplex mode on printer:

```
    listlp -d */*.[ch] | lp -o sides=two-sided-short-edge
```

Generates PDF for a manual page on Linux:

```
    gunzip < /usr/share/man/man1/ls.1.gz | nroff -man | listlp -1bn -fHelvetica > ls.pdf
```

Authors
-------

Pierre David (pdagog /at/ gmail.com)
and
Janick Taillandier

License
-------

CeCILL-B (see [http://cecill.info/licences/Licence_CeCILL-B_V1-en.html](http://cecill.info/licences/Licence_CeCILL-B_V1-en.html))

Todo
----

  * implement `-i` option
  * reduce margins with PDF output
  * use UTF-8 as native encoding instead of ISO-8859-1
